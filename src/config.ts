type Configuration = import("@/_boot/server").Configuration & import("@/_boot/database").Configuration;

const config: Configuration = {
  http: {
    host: process.env.HOST || "localhost",
    port: Number(process.env.PORT) || 3000,
  },
  mongodb: {
    database: process.env.DB_NAME || "todo",
    host: process.env.DB_HOST || "mongodb://localhost:27017",
    username: process.env.DB_USER || "todo",
    password: process.env.DB_PASS || "todo",
  },
};

export { config, Configuration };
