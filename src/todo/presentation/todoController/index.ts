import { makeDeleteTodo } from '@/todo/application/DeleteTodo';
import { Router } from 'express';
import { createTodoHandler } from "./CreateTodoHandler";
import { deleteTodoHandler } from './DeleteTodoHandler';
import { findTodosHandler } from "./FindTodosHandle";
import { updateTodoHandler } from './UpdateTodoHandler';

type Dependencies = {
    apiRouter: Router;
}
const makeTodoController = ({apiRouter}: Dependencies) => {
    const router = Router();
    
    router.post('/todos', createTodoHandler);
    router.get('/todos', findTodosHandler);
    router.put('/todos/:todoId', updateTodoHandler);
    router.delete('/todos/:todoId', deleteTodoHandler);

    apiRouter.use(router);
};

export { makeTodoController };