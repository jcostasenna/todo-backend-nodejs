import { DeleteTodo } from "@/todo/application/DeleteTodo";
import { controller } from "@/_lib/wrappers/controller";

type Dependencies = {
  deleteTodo: DeleteTodo;
};

const deleteTodoHandler = controller(
  ({ deleteTodo }: Dependencies) =>
    async (req, res) => {
      const { todoId } = req.params;

      console.log(todoId)
      await deleteTodo(todoId);

      res.sendStatus(204);
    }
);

export { deleteTodoHandler };
