import { FindTodo } from '@/todo/application/FindTodo';
import { controller } from '@/_lib/wrappers/controller';
import { Response, Request } from 'express';

type Dependencies = {
    findTodos: FindTodo;
}

const findTodosHandler = controller(({ findTodos}: Dependencies) => async (req: Request, res: Response) => {
    const todos = await findTodos();

    res.json(todos);

})

export { findTodosHandler };