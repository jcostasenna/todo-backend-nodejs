import  Joi  from 'joi';
import { CreateTodo } from "@/todo/application/CreateTodo";
import { makeValidator } from "@/_lib/validation/Validator";
import {Request, Response } from 'express';
import { controller } from '@/_lib/wrappers/controller';

type Dependencies = {
    createTodo: CreateTodo; 
}

const { getBody } = makeValidator({
    body: Joi.object({
        title: Joi.string().required(),
    }).required(),
});

const createTodoHandler = controller(
    ({ createTodo }: Dependencies) => 
    async (req: Request, res: Response) => {
        const {title} = await getBody(req); 
        const todoId = await createTodo({ title}); 
        
        res.json({ id: todoId});
    }
);

    export {createTodoHandler}