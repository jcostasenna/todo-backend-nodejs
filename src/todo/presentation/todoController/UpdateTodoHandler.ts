import { UpdateTodo } from "@/todo/application/ChangeTitleTodo";
import {Request, Response } from 'express';

type Dependencies = {
    updateTodo: UpdateTodo; 
}

const updateTodoHandler = ({ updateTodo }: Dependencies) => 
    async (req: Request, res: Response) => {
        const {todoId} = req.params; 
        const {body} = req;

         await updateTodo(todoId, body); 
        
        res.sendStatus(204);
    }

    export {updateTodoHandler}