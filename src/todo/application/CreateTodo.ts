import { TodoRepository } from "../domain/TodoRepository";
import { Todo } from '../domain/Todo';

type Dependencies = {
    todoRepository: TodoRepository;
}


type CreateTodoDTO = {
    title: string;

}

const makeCreateTodo = ({ todoRepository }: Dependencies) => 
    async (payload: CreateTodoDTO) => {
        const id = await todoRepository.getNextId();

    const todo = Todo.create({
        id,
        title: payload.title,
    });

    await todoRepository.store(todo);
    
    return id;
}

type CreateTodo = ReturnType<typeof makeCreateTodo>;

export {makeCreateTodo, CreateTodo }