import { Todo } from '@/todo/domain/Todo';
import { TodoRepository } from '@/todo/domain/TodoRepository';

type Dependencies = {
    todoRepository: TodoRepository
};

const makeUpdateTodo = ({todoRepository}: Dependencies) =>
    async (payload: string, newTitle: string) => {
        let todo = await todoRepository.findById(payload);

        todo = Todo.changeTitle(todo, newTitle);

        await todoRepository.store(todo);

    }
    type UpdateTodo = ReturnType<typeof makeUpdateTodo>;

    export {makeUpdateTodo, UpdateTodo }