import { TodoRepository } from '../domain/TodoRepository';
import { Todo } from '../domain/Todo';

type Dependencies = {
    todoRepository: TodoRepository;
}

const makeDeleteTodo =
({ todoRepository}: Dependencies) => 
async (payload: string) => {
    let todo = await todoRepository.findById(payload);

    todo = Todo.markAsDeleted(todo);

    await todoRepository.store(todo);
};

export type DeleteTodo = ReturnType<typeof makeDeleteTodo>;

export {makeDeleteTodo};