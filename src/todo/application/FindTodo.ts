import { QueryHandler, QueryResult } from '@/_lib/CQRS.d';

type TodoListItemDTO = Readonly<{
    id: string;
    title: string;
}>;

type FindTodo = QueryHandler<void, QueryResult<TodoListItemDTO[]>>

export { FindTodo };