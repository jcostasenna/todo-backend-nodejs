import { makeWithInvariants } from "@/_lib/WithInvariants";

namespace Todo {


type Todo = Readonly<{
    id: string; 
    title: string;
    status: "Active" | "Done" | "Deleted";
    createdAt: Date;
    updatedAt: Date;
    version: number;
}>;

const withInvariants = makeWithInvariants<Todo>((self, assert) => {
    assert(self.title?.length > 0);
});

type TodoProps = Readonly<{
    id: string;
    title:  string;
}>

export const create = withInvariants(
    (props: TodoProps): Todo => ({
        id: props.id,
        title: props.title,
        status: "Active",
        createdAt: new Date(),
        updatedAt: new Date(),
        version: 0,
})
);

export const changeTitle = withInvariants(
    (self: Todo, title: string): Todo => ({
    ...self,
    title,
})
);

export const markAsDone = withInvariants(
    (self: Todo): Todo => ({
        ...self,
        status: "Done",
        updatedAt: new Date(),
    })
)

export const markAsDeleted = withInvariants(
    (self: Todo): Todo => ({
        ...self,
        status: "Deleted"
      })
)

export type Type = Todo;


}

export { Todo };


