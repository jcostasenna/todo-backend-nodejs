import { Todo } from "@/todo/domain/Todo";

type TodoRepository = {
    getNextId(): Promise<string>;
    findById(id: string): Promise<Todo.Type>;
    store(entity: Todo.Type): Promise<void>;
}

export { TodoRepository }

