import { FindTodo } from "@/todo/application/FindTodo";
import MUUID from "uuid-mongodb";
import { TodoCollection, TodoSchema} from '@/todo/infraestructure/TodoCollection';

type Dependencies = {
  todoCollection: TodoCollection;
};

const makeMongoFindTodos =
  ({ todoCollection }: Dependencies): FindTodo =>
  async () => {
      const todos = await todoCollection
      .aggregate([
        {
          $match: {
            status: "Active",
            deleted: false,
          },
        }
      ])
      .toArray<TodoSchema>();

    return {
      data: todos.map(todo => ({
        id: MUUID.from(todo._id).toString(),
        title: todo.title,
      })),
    };
  };

export { makeMongoFindTodos };
