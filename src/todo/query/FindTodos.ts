import { MemoryDB } from '@/todo/infraestructure/MemoryDB';

type Dependencies = {
    memoryDB: MemoryDB;
}

type TodoListItemDTO = {
    id: string;
    title: string;
    status: "Active" | "Done" | "Delete";
}

const makeFindTodos = ({ memoryDB }: Dependencies) => 
async (): Promise<TodoListItemDTO[]> => {
    const todos = Object.values(memoryDB.todos);

    return todos.map(todo => ({
        id: todo.id.toString(),
        title: todo.title,
        status: todo.status
    }));
}

export type FindTodos = ReturnType<typeof makeFindTodos>;

export { makeFindTodos };