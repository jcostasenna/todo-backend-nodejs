import { Todo } from '@/todo/domain/Todo';
import { TodoRepository } from '@/todo/domain/TodoRepository';
import { TodoCollection } from "@/todo/infraestructure/TodoCollection";
import MUUID from 'uuid-mongodb';


type Dependencies = {
    todoCollection: TodoCollection;
}

const makeMongoTodoRepository = ({ todoCollection }: Dependencies): TodoRepository => ({
    async getNextId(): Promise<string> {
        return Promise.resolve(MUUID.v4().toString());
    },
    async findById(id: string): Promise<Todo.Type> {
        const todo = await todoCollection.findOne({_id: MUUID.from(id), deleted: false});

        if(!todo) {
            throw new Error('Todo not found');
        }

        return {
            id: MUUID.from(todo._id).toString(),
            title: todo.title,
            status: todo.status,
            createdAt: todo.createdAt,
            updatedAt: todo.updatedAt,
            version: todo.version
        };
    },
    async store(entity: Todo.Type): Promise<void> {
        const count = await todoCollection.countDocuments({ _id: MUUID.from(entity.id), deleted: false});

        if(count) {
            await todoCollection.updateOne(
                {_id: MUUID.from(entity.id), version: entity.version},
                {
                    $set: {
                        title: entity.title,
                        status: entity.status,
                        createdAt: entity.createdAt,
                        deleted: entity.status === "Deleted",
                        updatedAt: new Date(),
                        version: entity.version + 1,
                    },
                }
            );

            return;
        }
        await todoCollection.insertOne({
            _id: MUUID.from(entity.id),
            title: entity.title,
            status: entity.status,
            createdAt: entity.createdAt,
            deleted: entity.status === "Deleted",
            updatedAt: entity.updatedAt,
            version: entity.version,
        });
    }
}) 

export { makeMongoTodoRepository };
