import { UpdateTodo } from '@/todo/application/ChangeTitleTodo';
import { MemoryDB } from '@/todo/infraestructure/MemoryDB';
import { Todo } from '@/todo/domain/Todo';
import { TodoRepository  } from '@/todo/domain/TodoRepository';
import MUIID from 'uuid-mongodb';

type Dependencies = {
    memoryDB: MemoryDB
};

const makeInMemoryTodoRepository = ({ memoryDB }: Dependencies): TodoRepository => {
    return {
        async getNextId(): Promise<string> {
            return Promise.resolve(MUIID.v4().toString());
        },
        async findById(id: string): Promise<Todo.Type> {
            const todo = memoryDB.todos[id];
        
            if(!todo) {
                throw new Error(`Task not found for id ${id}`);
            }

            return Promise.resolve(todo);
        },
        async store(todo: Todo.Type): Promise<void> {
            memoryDB.todos[todo.id] = todo;

            return Promise.resolve();
        }
    }
}

export { makeInMemoryTodoRepository };