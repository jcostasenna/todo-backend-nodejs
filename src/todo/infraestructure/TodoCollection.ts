import { Collection, Db } from "mongodb";
import { MUUID }  from 'uuid-mongodb';

type TodoSchema = {
    _id: string | MUUID;
    title: string;
    status : "Active" | "Done" | "Deleted";
    createdAt: Date;
    deleted: boolean;
    updatedAt: Date;
    version: number;
}

type TodoCollection = Collection<TodoSchema>;

const initTodoCollection = async (db: Db): Promise<TodoCollection> => {
    const collection: TodoCollection = db.collection("todo");

    await collection.createIndex({ title: 1}, {unique: true});
    await collection.createIndex({_id: 1, version: 1});
    await collection.createIndex({_id: 1, deleted: 1});

    return collection;
}

export {
    initTodoCollection,
    TodoSchema,
    TodoCollection
}