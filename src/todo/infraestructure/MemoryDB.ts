import { Todo } from '@/todo/domain/Todo';

type MemoryDB = {
    todos: Record<string, Todo.Type>
};

const makeMemoryDB = (): MemoryDB => {
    return {
        todos: {},
    };
};

export { makeMemoryDB, MemoryDB };