import { FindTodo } from '@/todo/application/FindTodo';
import { initTodoCollection, TodoCollection } from '@/todo/infraestructure/TodoCollection';
import { UpdateTodo } from '@/todo/application/ChangeTitleTodo';
import { TodoRepository } from '@/todo/domain/TodoRepository';
import { initFunction } from "@/_lib/AppInitializer";
import { makeUpdateTodo } from '@/todo/application/ChangeTitleTodo';
import { CreateTodo, makeCreateTodo } from "@/todo/application/CreateTodo";
import { asFunction } from "awilix";
import { makeTodoController } from './presentation/todoController';
import { makeMongoTodoRepository } from './infraestructure/MongoTodoRepository';
import { withMongoProvider } from '@/_lib/MongoProvider';
import { toContainerValues } from '@/_lib/wrappers/toContainerValues';
import { DeleteTodo, makeDeleteTodo } from './application/DeleteTodo';
import { makeMongoFindTodos } from './query/impl/MongoFindTodos';

const todoModule = initFunction(async ({register, build}) => {
  const collections = await build(
    withMongoProvider({
    todoCollection: initTodoCollection,
    })
  );
  register({
    ...toContainerValues(collections),
    todoRepository: asFunction(makeMongoTodoRepository),
    createTodo: asFunction(makeCreateTodo),
    deleteTodo: asFunction(makeDeleteTodo),
    findTodos: asFunction(makeMongoFindTodos),
    updateTodo: asFunction(makeUpdateTodo),
  });
  build(makeTodoController);

});


type Container = {
  todoCollection: TodoCollection,
  todoRepository: TodoRepository,
  createTodo: CreateTodo,
  findTodos: FindTodo,
  updateTodo: UpdateTodo,
  deleteTodo: DeleteTodo,
};

export { todoModule, Container };