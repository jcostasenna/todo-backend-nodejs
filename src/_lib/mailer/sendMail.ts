import nodemailer from 'nodemailer';

const transporter = nodemailer.createTransport({
  host: 'http://vps-6452200.threewise.com.br/',
  port: 587,
  secure: false,
  tls: {
    rejectUnauthorized: false,
  },
  auth: {
    user: 'no-reply@politecnicabr.com.br',
    pass: 'nWPre6KTDspTXC',
  },
});

interface Email {
  to: string;
  from?: string;
  subject: string;
  html: string;
}

async function sendMail(email: Email) {
  return transporter.sendMail({
    from: 'PolitécnicaBR <no-reply@politecnicabr.com.br>',
    ...email,
  });
}

export default sendMail;
